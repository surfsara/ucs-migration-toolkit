#
# This file is part of ucs-migration-toolkit
#
# ucs-migration-toolkit is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ucs-migration-toolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ucs-migration-toolkit.  
# 
# If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2016-2017 SURFsara

# Python imports
import sys
import base64
import time
import ldif
import subprocess

# Ucsmt imports
from ucsmt import common
from ucsmt import umd
from ucsmt import tools
from ucsmt.connectors import get_module

class UcsmtException(Exception):
    pass

class Ucsmt(object):

    __special_func = {
        'base64encode': tools.to_base64,
        'split': tools.split,
        'append': tools.append,
        'replace': tools.replace
    }

    def __init__(self, **kwargs):
        self.cmn = common.Common(**kwargs)

        self.connector = get_module(self.cmn.connector())
        self.con = self.connector.Connector(
            self.cmn.cfg.get('source', 'uri'),
            self.cmn.cfg.get('source', 'bind_dn'),
            self.cmn.cfg.get('source', 'bind_pwd')
        )
        self.umc = umd.UniventionDirectoryManager()

    def run(self):

        source_data = self.con.get_data(self.cmn)

        p = subprocess.Popen('/usr/share/univention-heimdal/kerberos_now', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        print stdout        
        if stderr:
            print "ERROR", stderr
            sys.exit(1)

        if self.cmn.cfg.has_section('source_filters') and self.cmn.cfg.has_option('source_filters', 'users'):
            if 'users' in source_data:
                self.run_users(source_data['users'])

        p = subprocess.Popen('/usr/share/univention-heimdal/kerberos_now', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        print stdout        
        if stderr:
            print "ERROR", stderr
    
        if self.cmn.cfg.has_section('source_filters') and self.cmn.cfg.has_option('source_filters', 'groups'):
            if 'groups' in source_data:
                self.run_groups(source_data['groups'])

    def run_users(self, data):
        ucs_users = self.umc.get_users()

        for user in data['data']:
            sdata = user[1]
            name = sdata[data['identifier']][0]
            dn = "uid=%s,%s" % (
                name, self.cmn.cfg.get('DEFAULT', 'users_dn')
            )
            user_data = dict()

            if self.cmn.is_blacklisted('users', name):
                continue

            passwords = dict()
            for attr in self.cmn.attributes('users'):
                sattr = self.cmn.get_attribute('users', attr)

                if sattr not in sdata:
                    continue

                if self.cmn.attribute_is_special('users', attr):
                    special, args = self.cmn.get_special('users', attr)
                    
                    if len(sdata[sattr]) == 1:
                        user_data[attr] = self.__special_func[special](sdata[sattr][0], args)
                    else:
                        user_data[attr] = [ self.__special_func[special](sdata[sattr][0], x) for x in sdata[sattr] ]
                elif attr in ['userPassword', 'sambaNTPassword']:
                    passwords[attr] = sdata[sattr][0]
                else:
                    if len(sdata[sattr]) == 1:
                        user_data[attr] = sdata[sattr][0]
                    else:
                        user_data[attr] = sdata[sattr]

            user_data['password'] = tools.gen_password()

            if self.cmn.has_default_values('users'):
                for key, value in self.cmn.get_default_values('users').items():
                    user_data[key] = value


            if name in ucs_users.keys():
                self.umc.users('modify', self.cmn.cfg.get('DEFAULT', 'users_dn'), dn, user_data)
            else:
                self.umc.users('create', self.cmn.cfg.get('DEFAULT', 'users_dn'), dn, user_data)

            if len(passwords) == 2:
                self.umc.change_userpassword(dn, passwords['userPassword'], passwords['sambaNTPassword'])
            else:
                print '   Unable to adjust password for user'


    def run_groups(self, data):
        ucs_groups = self.umc.get_groups().keys()
        ucs_users = self.umc.get_users()

        for group in data['data']:
            sdata = group[1]
            name = sdata[data['identifier']][0]
            dn = "cn=%s,%s" % (
                name, self.cmn.cfg.get('DEFAULT', 'groups_dn')
            )
            group_data = dict()

            if self.cmn.is_blacklisted('groups', name):
                continue

            if name in ucs_users.keys():
                continue

            for attr in self.cmn.attributes('groups'):
                sattr = self.cmn.get_attribute('groups', attr)

                if sattr not in sdata:
                    continue

                if self.cmn.attribute_is_special('groups', attr):
                    special, args = self.cmn.get_special('groups', attr)
                    
                    if len(sdata[sattr]) == 1:
                        group_data[attr] = self.__special_func[special](sdata[sattr][0], args)
                    else:
                        group_data[attr] = [ self.__special_func[special](sdata[sattr][0], x) for x in sdata[sattr] ]
                elif attr == 'users':
                    smembers = sdata[sattr]
                    members = list()
                    for member in smembers:
                        if member in ucs_users.keys():
                            members.append(ucs_users[member])

                    if members:
                        group_data[attr] = members
                else:
                    if len(sdata[sattr]) == 1:
                        group_data[attr] = sdata[sattr][0]
                    else:
                        group_data[attr] = sdata[sattr]

            if name in ucs_groups:
                self.umc.groups('modify', self.cmn.cfg.get('DEFAULT', 'groups_dn'), dn, group_data)
            else:
                self.umc.groups('create', self.cmn.cfg.get('DEFAULT', 'groups_dn'), dn, group_data)
