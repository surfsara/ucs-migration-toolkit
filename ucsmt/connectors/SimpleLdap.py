#
# This file is part of ucs-migration-toolkit
#
# ucs-migration-toolkit is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ucs-migration-toolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ucs-migration-toolkit.  
# 
# If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2016-2017 SURFsara

# Python imports
import os

# python-ldap imports
import ldap

# Univention imports
from ucsmt.connectors import BaseConnector

class ConnectorException(Exception):
    pass

class Connector(BaseConnector):

    __ldap = None
    __scopes = {
        'base': ldap.SCOPE_BASE,
        'onelevel': ldap.SCOPE_ONELEVEL,
        'subordinate': ldap.SCOPE_SUBORDINATE,
        'subtree': ldap.SCOPE_SUBTREE
    }

    def __init__(self, uri, bind_dn=None, bind_pwd=None):

        if not self.__ldap:
            self.__ldap = ldap.initialize(uri)


        if bind_pwd and os.path.exists(bind_pwd):
            password = open(bind_pwd, 'r').read().strip()
        elif bind_pwd:
            password = bind_pwd

        if bind_dn and bind_pwd:
            self.__ldap.simple_bind_s(bind_dn, password)

    def get_entries(self, base, scope, lfilter, attributes):
        if not scope in self.__scopes:
            raise ConnectorException("Given scope '%s' is invalid" % scope)

        return self.__ldap.search_s(base, self.__scopes[scope], lfilter, attributes)

    def get_data(self, cmn):

        if not cmn.cfg.has_section('source_filters'):
            raise ConnectorException('Unable to continue missing section source_filters in the configuration')

        ldap_data = dict()
        for srcfilter in cmn.filters():
            attributes = cmn.mapping_attributes(srcfilter)

            parts = cmn.cfg.get('source_filters', srcfilter).split(';')
            if len(parts) != 3:
                raise ConnectorException('You will need to configure at least 3 options as value, see configuration file for the options')

            if not parts[1].strip():
                parts[1] = '(objectClass=*)'

            ldap_data[srcfilter] = dict()
            ldap_data[srcfilter]['identifier'] = parts[2]
            ldap_data[srcfilter]['data'] = self.get_entries(
                parts[0], 'onelevel', parts[1], attributes
            )
        return ldap_data

