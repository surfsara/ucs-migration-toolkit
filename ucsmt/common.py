#
# This file is part of ucs-migration-toolkit
#
# ucs-migration-toolkit is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ucs-migration-toolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ucs-migration-toolkit.  
# 
# If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2016-2017 SURFsara

# Python imports
import os
from ConfigParser import SafeConfigParser

class CommonException(Exception):
    pass

class Common(object):

    _required_options = set(['config_file'])

    def __init__(self, **kwargs):
        _got = self._required_options.intersection(kwargs)

        if len(self._required_options) != len(_got):
            raise CommonException("Missing some required options: %s" % ", ".join(
                self._required_options.difference(_got)
            ))

        # Perhaps not so good idea?, but for now it's okayish
        for option, value in kwargs.items():
            setattr(self, option, value)

        if not os.access(self.config_file, os.R_OK):
            raise CommonException("Unable to continue, could not locate config file '%s'" % self.config_file)

        self.cfg = SafeConfigParser()
        self.cfg.optionxform = str
        self.cfg.read([self.config_file])

    def connector(self):
        if not self.cfg.has_option('source', 'connector'):
            raise CommonException('Unable to locate connector option in section source')
        return self.cfg.get('source', 'connector')

    def filters(self):
        filters = list()
        for filtr in self.cfg.options('source_filters'):
            if self.cfg.has_option('DEFAULT', filtr):
                continue
            filters.append(filtr)
        return filters

    def mapping_attributes(self, lfilter):

        if not self.cfg.has_section('%s_mapping' % lfilter):
            return None

        attributes = list()
        for key in self.cfg.options('%s_mapping' % lfilter):
            if self.cfg.has_option('DEFAULT', key):
                continue
            attributes.extend(self.cfg.get('%s_mapping' % lfilter, key).split(','))
        return attributes

    def attributes(self, lfilter):
        if not self.cfg.has_section('%s_mapping' % lfilter):
            return None
        
        attributes = list()
        for key in self.cfg.options('%s_mapping' % lfilter):
            if self.cfg.has_option('DEFAULT', key):
                continue
            attributes.append(key)
        return attributes

    def get_attribute(self, lfilter, attribute):
        if not self.cfg.has_section('%s_mapping' % lfilter):
            return None

        if not self.cfg.has_option('%s_mapping' % lfilter, attribute):
            return None

        return self.cfg.get('%s_mapping' % lfilter, attribute)

    def attribute_is_special(self, module, attribute):
        if self.cfg.has_option('%s_special_attributes' % module, attribute):
            return True
        return False

    def get_special(self, module, attribute):

        if not self.attribute_is_special(module, attribute):
            return None

        parts = self.cfg.get('%s_special_attributes' % module, attribute).split(':')
        return parts[0], parts[1]

    def is_blacklisted(self, sfilter, name):

        if name in self.cfg.get('blacklist', sfilter).split(','):
            return True

        if sfilter == 'groups':
            if name in self.cfg.get('blacklist', 'users').split(','):
                return True

        return False

    def has_default_values(self, module):
        if self.cfg.has_section('%s_default_values' % module):
            return True
        return False

    def get_default_values(self, module):
        default_values = dict()
        for option in self.cfg.options('%s_default_values' % module):
            if self.cfg.has_option('DEFAULT', option):
                continue
            default_values[option] = self.cfg.get('%s_default_values' % module, option)
        return default_values
