#
# This file is part of ucs-migration-toolkit
#
# ucs-migration-toolkit is free software: you can redistribute it and/or 
# modify it under the terms of the GNU General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ucs-migration-toolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ucs-migration-toolkit.  
# 
# If not, see <http://www.gnu.org/licenses/>.
#
# Copyright 2016-2017 SURFsara

# Python imports
import os
import subprocess
import re

# python-ldap
import ldap

# univention imports
import univention.config_registry

class UniventionException(Exception):
    pass

class UniventionDirectoryManager(object):
    '''For now I use the shell command umd, but perhaps in the future make use of the univention Puthon modules'''

    __command = '/usr/sbin/udm'
    __actions = ['create', 'modify', 'remove', 'list', 'move']
    __modules = ['users/user', 'groups/group' ]

    def __init__(self):
        self.ucr = univention.config_registry.ConfigRegistry()
        self.ucr.load()

        if not os.access(self.ucr['samba/user/pwdfile'], os.R_OK):
            raise UniventionException('Unable to read or find the %s file' % self.ucr['samba/user/pwdfile'])

        self.ldap_password = open(self.ucr['samba/user/pwdfile']).read()

    def run(self, module, action, args=list(), shell=False):
        if not module in self.__modules:
            raise UniventionException("Module '%s' is not valid" % module)

        if not action in self.__actions:
            raise UniventionException("Action '%s' is not valid" % action)

        command = [
            self.__command, module, action
        ]
        command.extend(args)

        if shell:
            command = " ".join(command)

        p = subprocess.Popen(command, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()

        return p.returncode, stdout, stderr

    def get_users(self):
        '''Get all users from the UCS'''
        rcode, stdout, stderr = self.run('users/user', 'list')

        uids = dict()
       
        if rcode != 0:
            return uids 

        user_dn = None
        for line in stdout.splitlines():
            parts = line.strip().split(':')
            if parts[0] == 'DN':
                user_dn = parts[1].strip()
            elif len(parts) == 2 and user_dn and parts[0] == 'username':
                uids[parts[1].strip()] = user_dn
                user_dn = None

        return uids

    def get_groups(self):
        '''Get all groups from the UCS'''
        rcode, stdout, stderr = self.run('groups/group', 'list')

        gids = dict()
       
        if rcode != 0:
            return uids 
        
        group_dn = None
        for line in stdout.splitlines():
            parts = line.strip().split(':')
            if parts[0] == 'DN':
                group_dn = parts[1].strip()
            elif len(parts) == 2 and group_dn and parts[0] == 'name':
                gids[parts[1].strip()] = group_dn
                group_dn = None

        return gids

    def generate_umd_command(self, module, data):
        '''Generates a umd command for create/modify (aka set), and append, which is an list of commands'''

        command_set = list()
        command_append = list()

        for key, value in data.items():
            if type(value) == type(list()):
                command_set.append('--set %s="%s"' % (key, value[0]))

                for v in value[1:]:
                    command_append.append('--append %s="%s"' % (key, v))
            else:
                command_set.append('--set %s="%s"' % (key, value))

        return {'set': command_set, 'append': command_append }

    def users(self, mode, base_dn, dn, data):
        umd_command_args = self.generate_umd_command('users', data)

        if mode == 'create':
            args_set = ['--position "%s"' % base_dn,] + umd_command_args['set']
        elif mode == 'modify':
            args_set = ['--dn "%s"' % dn,] + umd_command_args['set']
        else:
            raise univentionexception("given mode '%s' is not supported" % mode)

        rcode, stdout, stderr = self.run('users/user', mode, args_set, shell=True)

        if rcode == 0:
            print dn, stdout.strip()
        else:
            print dn, stdout.strip()
            print dn, stderr.strip()
       
        for append in umd_command_args['append']:
            rcode, stdout, stderr = self.run('users/user', 'modify', ['--dn "%s"' % dn, append], shell=True)
            if rcode == 0:
                print '    Append', stdout.strip()
            else:
                print '    Apennd', stdout.strip()
                print '    Append', stderr.strip()

        print


    def groups(self, mode, base_dn, dn, data):
        umd_command_args = self.generate_umd_command('groups', data)

        if mode == 'create':
            args_set = ['--position "%s"' % base_dn,] + umd_command_args['set']
        elif mode == 'modify':
            args_set = ['--dn "%s"' % dn,] + umd_command_args['set']
        else:
            raise univentionexception("given mode '%s' is not supported" % mode)

        rcode, stdout, stderr = self.run('groups/group', mode, args_set, shell=True)

        if rcode == 0:
            print dn, stdout.strip()
        else:
            print dn, stdout.strip()
            print stderr.strip()

        ## We need to catch this error to skip the append action
        if re.findall(r'(is already in use)', stdout.strip()):
            skip_append = True
        else:
            skip_append = False

        ## next step! is append and this is always a modify!
        if not skip_append:
            for append in umd_command_args['append']:
                rcode, stdout, stderr = self.run('groups/group', 'modify', ['--dn "%s"' % dn, append], shell=True)
                if rcode == 0:
                    print '    Append', stdout.strip()
                else:
                    print '    Apennd', stdout.strip()
                    print '    Append', stderr.strip()

        print

    def change_userpassword(self, dn, userPassword, sambaNTPassword):

        ## Yes I know, not best practise
        ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
        l = ldap.initialize('ldaps://127.0.0.1:7636')
        l.bind_s(self.ucr['samba/user'], self.ldap_password)

        mod_attrs = [
            (ldap.MOD_REPLACE, 'userPassword', [userPassword]),
            (ldap.MOD_REPLACE, 'sambaNTPassword', [sambaNTPassword]),
            (ldap.MOD_DELETE, 'krb5PrincipalName', None),
            (ldap.MOD_DELETE, 'krb5MaxLife', None),
            (ldap.MOD_DELETE, 'krb5MaxRenew', None),
            (ldap.MOD_DELETE, 'krb5KDCFlags', None),
            (ldap.MOD_DELETE, 'krb5KeyVersionNumber', None),
            (ldap.MOD_DELETE, 'krb5Key', None),
            (ldap.MOD_DELETE, 'objectClass', ['krb5Principal']),
            (ldap.MOD_DELETE, 'objectClass', ['krb5KDCEntry'])
        ]
        l.modify_s(dn, mod_attrs)

        l.unbind_s()
