# The UCS migration toolkit
This is a toolkit which allows you to migrate from a OpenLDAP environment to UCS

## The configuration
For now read the configuration file, documentation will come later

## How to run
```
./bin/ucsmt -c files/ucsmt.cfg
```
